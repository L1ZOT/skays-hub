local GuiService = game:GetService("GuiService")
local UserInputService = game:GetService("UserInputService")

if (GuiService:IsTenFootInterface()) then
    return nil
elseif (UserInputService.TouchEnabled and not UserInputService.MouseEnabled) then
    local DeviceSize = workspace.CurrentCamera.ViewportSize; 
	return loadstring(game:HttpGet("https://gitlab.com/L1ZOT/skays-hub/-/raw/main/Testers/Mobile-Gui.lua"))()
else
    return loadstring(game:HttpGet("https://gitlab.com/L1ZOT/skays-hub/-/raw/main/Testers/Pc-Gui.lua"))()
end
